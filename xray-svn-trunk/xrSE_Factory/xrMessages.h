#ifndef _INCDEF_XRMESSAGES_H_
#define _INCDEF_XRMESSAGES_H_

#pragma once

// CL	== client 2 server message
// SV	== server 2 client message

enum {
	M_UPDATE			= 0,	// DUAL: Update state
	M_SPAWN,					// DUAL: Spawning, full state

	M_SV_CONFIG_NEW_CLIENT,
	M_SV_CONFIG_GAME,
	M_SV_CONFIG_FINISHED,

	M_EVENT,					// Game Event
	//----------- for E3 -----------------------------
	M_CL_UPDATE,

	M_CHANGE_LEVEL,				// changing level
	M_LOAD_GAME,
	M_SAVE_GAME,
	M_SAVE_PACKET,

	M_EVENT_PACK,					// Pack of M_EVENT

	M_CLIENT_REQUEST_CONNECTION_DATA,

	MSG_FORCEDWORD				= u32(-1)
};

enum {
	GE_OWNERSHIP_TAKE,			// DUAL: Client request for ownership of an item
	GE_OWNERSHIP_TAKE_MP_FORCED,
	GE_OWNERSHIP_REJECT,		// DUAL: Client request ownership rejection

	GE_HIT,						//
	GE_DIE,						//
	GE_ASSIGN_KILLER,			//
	GE_DESTROY,					// authorative client request for entity-destroy
	GE_TELEPORT_OBJECT,

	GE_ADD_RESTRICTION,
	GE_REMOVE_RESTRICTION,
	GE_REMOVE_ALL_RESTRICTIONS,

	GE_INFO_TRANSFER,			//transfer _new_ info on PDA
	
	GE_TRADE_SELL,
	GE_TRADE_BUY,

	GE_WPN_STATE_CHANGE,

	GE_ADDON_ATTACH,
	
	GE_GRENADE_EXPLODE,
	GE_INV_ACTION,				//a action beign taken on inventory

	GE_ZONE_STATE_CHANGE,

	GE_CHANGE_POS,

	GE_GAME_EVENT,

	GE_CHANGE_VISUAL,
	GE_MONEY,

	GEG_PLAYER_WEAPON_HIDE_STATE,
	
	GEG_PLAYER_ATTACH_HOLDER,
	GEG_PLAYER_DETACH_HOLDER,

	GE_KILL_SOMEONE,

	GE_FORCEDWORD				= u32(-1)
};


enum EGameMessages {  //game_cl <----> game_sv messages
	GAME_EVENT_PLAYER_READY,	
	GAME_EVENT_PLAYER_KILL,			//player wants to die

	GAME_EVENT_PLAYER_GAME_MENU,
	GAME_EVENT_PLAYER_GAME_MENU_RESPOND,	

	GAME_EVENT_PLAYER_CONNECTED	,
	GAME_EVENT_PLAYER_DISCONNECTED	,
	GAME_EVENT_PLAYER_ENTERED_GAME	,

	GAME_EVENT_PLAYER_HITTED,
	
	GAME_EVENT_ARTEFACT_SPAWNED		,
	GAME_EVENT_ARTEFACT_DESTROYED		,
	GAME_EVENT_ARTEFACT_TAKEN			,
	GAME_EVENT_ARTEFACT_DROPPED		,
	GAME_EVENT_ARTEFACT_ONBASE		,

	GAME_EVENT_PLAYER_ENTER_TEAM_BASE	,
	GAME_EVENT_PLAYER_LEAVE_TEAM_BASE	,

	GAME_EVENT_BUY_MENU_CLOSED		,
	GAME_EVENT_TEAM_MENU_CLOSED		,
	GAME_EVENT_SKIN_MENU_CLOSED		,

	GAME_EVENT_CREATE_CLIENT,
	GAME_EVENT_ON_HIT,
	GAME_EVENT_ON_TOUCH,

	GAME_EVENT_PLAYER_NAME,

	GAME_EVENT_SPEECH_MESSAGE,

	//-----------------------------------------
	GAME_EVENT_PLAYERS_MONEY_CHANGED,

	//-----------------------------------------
	GAME_EVENT_SCRIPT_BEGINS_FROM,		// don't add messages after this
	GAME_EVENT_FORCEDWORD				= u32(-1)
};

enum
{
	M_SPAWN_OBJECT_LOCAL		= (1<<0),	// after spawn it becomes local (authorative)
	M_SPAWN_OBJECT_HASUPDATE	= (1<<2),	// after spawn info it has update inside message
	M_SPAWN_OBJECT_ASPLAYER		= (1<<3),	// after spawn it must become viewable
	M_SPAWN_OBJECT_PHANTOM		= (1<<4),	// after spawn it must become viewable
	M_SPAWN_VERSION				= (1<<5),	// control version
	M_SPAWN_UPDATE				= (1<<6),	// + update packet
	M_SPAWN_TIME				= (1<<7),	// + spawn time

	M_SPAWN_OBJECT_FORCEDWORD	= u32(-1)
};

#endif /*_INCDEF_XRMESSAGES_H_*/
