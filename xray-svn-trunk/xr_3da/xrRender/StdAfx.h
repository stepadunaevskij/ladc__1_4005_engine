#pragma once

#if !defined(USE_DX10) && !defined(USE_DX11) //stdafx for r1 r2
#pragma warning(disable:4995)
#include "../../xr_3da/stdafx.h"
#pragma warning(disable:4995)
#include <d3dx9.h>
#pragma warning(default:4995)
#pragma warning(disable:4714)
#pragma warning( 4 : 4018 )
#pragma warning( 4 : 4244 )
#pragma warning(disable:4237)

#include "../xrRender/xrD3DDefs.h"

#include "HW.h"
#include "Shader.h"
#include "R_Backend.h"
#include "R_Backend_Runtime.h"
#endif

// For intelisence to work properly
#ifdef USE_DX10 //stdafx for r3
#include "../xrRender_R3/stdafx.h"
#elif USE_DX11 //stdafx for r4
#include "../xrRender_R4/stdafx.h"
#endif

