#include "pch_script.h"
#include "game_cl_base.h"
#include "script_engine.h"
#include "level.h"
#include "GamePersistent.h"
#include "UIGameSP.h"
#include "clsid_game.h"
#include "actor.h"

ESingleGameDifficulty g_SingleGameDifficulty = egdStalker;

xr_token	difficulty_type_token[] = {
	{ "gd_novice", egdNovice },
	{ "gd_stalker", egdStalker },
	{ "gd_veteran", egdVeteran },
	{ "gd_master", egdMaster },
	{ 0, 0 }
};

game_cl_GameState::game_cl_GameState()
{
	shedule.t_min				= 5;
	shedule.t_max				= 20;
	shedule_register			();
}

game_cl_GameState::~game_cl_GameState()
{
	shedule_unregister();
}

void game_cl_GameState::net_import_GameTime		(NET_Packet& P)
{
	u64				GameTime;
	P.r_u64			(GameTime);
	float			TimeFactor;
	P.r_float		(TimeFactor);

	Level().SetGameTimeFactor	(GameTime,TimeFactor);

	u64				GameEnvironmentTime;
	P.r_u64			(GameEnvironmentTime);
	float			EnvironmentTimeFactor;
	P.r_float		(EnvironmentTimeFactor);

	u64 OldTime = Level().GetEnvironmentGameTime();
	Level().SetEnvironmentGameTimeFactor	(GameEnvironmentTime,EnvironmentTimeFactor);
	if (OldTime > GameEnvironmentTime)
		GamePersistent().Environment().Invalidate();
}

void game_cl_GameState::net_import_state	(NET_Packet& P)
{
	P.r_s32			(game_type);

	ClientID			ID;
	P.r_clientID		(ID);

	net_import_GameTime(P);
}

void game_cl_GameState::net_import_update(NET_Packet& P)
{
	ClientID			ID;
	P.r_clientID		(ID);

	net_import_GameTime (P);
}

float game_cl_GameState::shedule_Scale		()
{
	return 1.0f;
}

void game_cl_GameState::shedule_Update		(u32 dt)
{
	ISheduled::shedule_Update	(dt);
};

CUIGameCustom* game_cl_GameState::createGameUI()
{
	CLASS_ID clsid = CLSID_GAME_UI_SINGLE;
	CUIGameSP*	pUIGame = smart_cast<CUIGameSP*> (NEW_INSTANCE(clsid));
	R_ASSERT(pUIGame);
	pUIGame->Load();
	pUIGame->Init(0);
	pUIGame->Init(1);
	pUIGame->Init(2);
	return					pUIGame;
}

void game_cl_GameState::sv_GameEventGen(NET_Packet& P)
{
	P.w_begin	(M_EVENT);
	P.w_u32		(Level().timeServer());
	P.w_u16		( u16(GE_GAME_EVENT&0xffff) );
	P.w_u16		(0);
}

void game_cl_GameState::sv_EventSend(NET_Packet& P)
{
	Level().Send(P,net_flags(TRUE,TRUE));
}

void game_cl_GameState::u_EventGen(NET_Packet& P, u16 type, u16 dest)
{
	P.w_begin	(M_EVENT);
	P.w_u32		(Level().timeServer());
	P.w_u16		(type);
	P.w_u16		(dest);
}

void game_cl_GameState::u_EventSend(NET_Packet& P)
{
	Level().Send(P,net_flags(TRUE,TRUE));
}

void game_cl_GameState::SendPickUpEvent(u16 ID_who, u16 ID_what)
{
	CObject* O		= Level().Objects.net_Find	(ID_what);
	Level().m_feel_deny.feel_touch_deny			(O, 1000);

	NET_Packet		P;
	u_EventGen		(P,GE_OWNERSHIP_TAKE, ID_who);
	P.w_u16			(ID_what);
	u_EventSend		(P);
};

//Game difficulty
void game_cl_GameState::OnDifficultyChanged()
{
	Actor()->OnDifficultyChanged();
}

//---Alife
#include "ai_space.h"
#include "alife_simulator.h"
#include "alife_time_manager.h"
ALife::_TIME_ID game_cl_GameState::GetGameTime()
{
	if (ai().get_alife() && ai().alife().initialized())
		return(ai().alife().time_manager().game_time());
	else
		return(inherited::GetGameTime());
}

float game_cl_GameState::GetGameTimeFactor()
{
	if (ai().get_alife() && ai().alife().initialized())
		return(ai().alife().time_manager().time_factor());
	else
		return(inherited::GetGameTimeFactor());
}

void game_cl_GameState::SetGameTimeFactor(const float fTimeFactor)
{
	Level().Server->Server_game_sv_base->SetGameTimeFactor(fTimeFactor);
}

ALife::_TIME_ID game_cl_GameState::GetEnvironmentGameTime()
{
	if (ai().get_alife() && ai().alife().initialized())
		return	(ai().alife().time_manager().game_time());
	else
		return	(inherited::GetEnvironmentGameTime());
}

float game_cl_GameState::GetEnvironmentGameTimeFactor()
{
	if (ai().get_alife() && ai().alife().initialized())
		return	(ai().alife().time_manager().time_factor());
	else
		return	(inherited::GetEnvironmentGameTimeFactor());
}

void game_cl_GameState::SetEnvironmentGameTimeFactor(const float fTimeFactor)
{
	if (ai().get_alife() && ai().alife().initialized())
		Level().Server->Server_game_sv_base->SetGameTimeFactor(fTimeFactor);
	else
		inherited::SetEnvironmentGameTimeFactor(fTimeFactor);
}

void game_cl_GameState::SetEnvironmentGameTimeFactor(ALife::_TIME_ID GameTime, const float fTimeFactor)
{
	if (ai().get_alife() && ai().alife().initialized())
	{
#pragma todo("SkyLoader to all: uncomment and fix it plz")
		//ai().alife().time_manager().set_game_time(GameTime);
		//ai().alife().time_manager().set_time_factor(fTimeFactor);
	}
	else
	{
		inherited::SetEnvironmentGameTimeFactor(GameTime, fTimeFactor);
	}
}

void game_cl_GameState::SetGameTimeFactor(ALife::_TIME_ID GameTime, const float fTimeFactor)
{
	if (ai().get_alife() && ai().alife().initialized())
	{
#pragma todo("SkyLoader to all: uncomment and fix it plz")
		//ai().alife().time_manager().set_game_time(GameTime);
		//ai().alife().time_manager().set_time_factor(fTimeFactor);
	}
	else
	{
		inherited::SetGameTimeFactor(GameTime, fTimeFactor);
	}
}

using namespace luabind;
#pragma optimize("s",on)
void CScriptGameDifficulty::script_register(lua_State *L)
{
	module(L)
		[
			class_<enum_exporter<ESingleGameDifficulty> >("game_difficulty")
			.enum_("game_difficulty")
			[
				value("novice", int(egdNovice)),
				value("stalker", int(egdStalker)),
				value("veteran", int(egdVeteran)),
				value("master", int(egdMaster))
			]
		];
}