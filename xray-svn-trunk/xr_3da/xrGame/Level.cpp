#include "pch_script.h"
#include "../environment.h"
#include "../igame_persistent.h"
#include "ParticlesObject.h"
#include "Level.h"
#include "xrServer.h"
#include "net_queue.h"
#include "game_cl_base.h"
#include "hudmanager.h"
#include "ai_space.h"
#include "Physics.h"
#include "ShootingObject.h"
#include "player_hud.h"
#include "Level_Bullet_Manager.h"
#include "script_process.h"
#include "script_engine.h"
#include "script_engine_space.h"
#include "team_base_zone.h"
#include "infoportion.h"
#include "date_time.h"
#include "space_restriction_manager.h"
#include "seniority_hierarchy_holder.h"
#include "client_spawn_manager.h"
#include "autosave_manager.h"
#include "level_graph.h"
#include "mt_config.h"
#include "phcommander.h"
#include "fast_entity_update.h"
#include "map_manager.h"
#include "level_sounds.h"
#include "car.h"
#include "trade_parameters.h"
#include "clsid_game.h"
#include "CustomTimersManager.h"

#include "debug_renderer.h"
#include "ai/stalker/ai_stalker.h"
#include <functional>

#ifdef DEBUG
#	include "level_debug.h"
#	include "ai/stalker/ai_stalker.h"
#	include "physicobject.h"
#	include "space_restrictor.h"
#	include "climableobject.h "
#	include "ui_base.h"
#	include "ai_debug.h"

extern Flags32	psAI_Flags;
#endif

#pragma todo("game is always single player")
bool IsGameTypeSingle(){return true;}
u32	GameID()
{
	return GAME_SINGLE;
}

extern BOOL	g_bDebugDumpPhysicsStep;
extern CAI_Space *g_ai_space;
extern void draw_wnds_rects();

int			psLUA_GCSTEP		= 10;
CPHWorld	*ph_world			= 0;

CLevel::CLevel():IPureClient	(Device.GetTimerGlobal())

{
	Server						= NULL;

	level_game_cl_base			= NULL;
	game_events					= xr_new <NET_Queue_Event>();

	game_configured				= FALSE;
	m_bGameConfigStarted		= FALSE;

	eChangeTrack				= Engine.Event.Handler_Attach	("LEVEL:PlayMusic",this);
	eEnvironment				= Engine.Event.Handler_Attach	("LEVEL:Environment",this);

	eEntitySpawn				= Engine.Event.Handler_Attach	("LEVEL:spawn",this);

	m_pBulletManager			= xr_new <CBulletManager>();
	m_map_manager				= xr_new <CMapManager>();

	m_seniority_hierarchy_holder= xr_new <CSeniorityHierarchyHolder>();

	m_level_sound_manager		= xr_new <CLevelSoundManager>();
	m_space_restriction_manager = xr_new <CSpaceRestrictionManager>();
	m_client_spawn_manager		= xr_new <CClientSpawnManager>();
	m_autosave_manager			= xr_new <CAutosaveManager>();
	#ifdef DRENDER
		m_debug_renderer			= xr_new <CDebugRenderer>();
	#endif
	#ifdef DEBUG
		m_level_debug				= xr_new <CLevelDebug>();
	#endif

	m_ph_commander				= xr_new <CPHCommander>();
	m_ph_commander_scripts		= xr_new <CPHCommander>();

	m_fast_updater				= xr_new <CFastEntityUpdater>();

	pStatGraphR = NULL;
	pStatGraphS = NULL;

	g_player_hud				= xr_new <player_hud>();
	g_player_hud->load_default();

	pCurrentControlEntity = NULL;

	Msg("%s", Core.Params);
}

CLevel::~CLevel()
{
	xr_delete			(g_player_hud);
	Msg							("- Destroying level");
	Engine.Event.Handler_Detach	(eEntitySpawn,	this);
	Engine.Event.Handler_Detach	(eEnvironment,	this);
	Engine.Event.Handler_Detach	(eChangeTrack,	this);

	if (ph_world)
	{
		ph_world->Destroy		();
		xr_delete				(ph_world);
	}

	// destroy PSs
	for (POIt p_it=m_StaticParticles.begin(); m_StaticParticles.end()!=p_it; ++p_it)
		CParticlesObject::Destroy(*p_it);
	m_StaticParticles.clear		();

	// Unload sounds
	// unload prefetched sounds
	sound_registry.clear		();

	// unload static sounds
	for (u32 i=0; i<static_Sounds.size(); ++i){
		static_Sounds[i]->destroy();
		xr_delete				(static_Sounds[i]);
	}
	static_Sounds.clear			();

	xr_delete					(m_level_sound_manager);
	xr_delete					(m_space_restriction_manager);
	xr_delete					(m_seniority_hierarchy_holder);
	xr_delete					(m_client_spawn_manager);
	xr_delete					(m_autosave_manager);

	#ifdef DRENDER
	xr_delete					(m_debug_renderer);
	#endif


	ai().script_engine().remove_script_process(ScriptEngine::eScriptProcessorLevel);
	xr_delete					(level_game_cl_base);
	xr_delete					(game_events);
	xr_delete					(m_pBulletManager);
	xr_delete					(pStatGraphR);
	xr_delete					(pStatGraphS);
	xr_delete					(m_ph_commander);
	xr_delete					(m_fast_updater);
	xr_delete					(m_ph_commander_scripts);

	ai().unload					();

	#ifdef DEBUG	
	xr_delete(m_level_debug);
	#endif

	xr_delete					(m_map_manager);

	// here we clean default trade params
	// because they should be new for each saved/loaded game
	// and I didn't find better place to put this code in
	CTradeParameters::clean		();
}

shared_str	CLevel::name		() const
{
	return						(m_name);
}

#pragma note("This can be used, we dont even need to code sound prefetcher")
void CLevel::PrefetchSound		(LPCSTR name)
{
	// preprocess sound name
	string_path					tmp;
	xr_strcpy					(tmp,name);
	xr_strlwr					(tmp);
	if (strext(tmp))			*strext(tmp)=0;
	shared_str	snd_name		= tmp;
	// find in registry
	SoundRegistryMapIt it		= sound_registry.find(snd_name);
	// if find failed - preload sound
	if (it==sound_registry.end())
		sound_registry[snd_name].create(snd_name.c_str(),st_Effect,sg_SourceType);
}

void CLevel::cl_Process_Event				(u16 dest, u16 type, NET_Packet& P)
{
	//			Msg				("--- event[%d] for [%d]",type,dest);
	CObject*	 O	= Objects.net_Find	(dest);
	if (O==NULL){
	#ifdef DEBUG
		Msg("* WARNING: c_EVENT[%d] to object: can't find object with id [%d]", type, dest);
	#endif // DEBUG
		return;
	}
	CGameObject* GO = smart_cast<CGameObject*>(O);
	if (!GO)		{
		Msg("! ERROR: c_EVENT[%d] to object: is not gameobject",dest);
		return;
	}

	GO->OnEvent		(P,type);

};

void CLevel::ProcessGameEvents		()
{
	// Game events
	{
		NET_Packet			P;
		u32 svT				= timeServer()-NET_Latency;

		while	(game_events->available(svT))
		{
			u16 ID,dest,type;
			game_events->get	(ID,dest,type,P);

			switch (ID)
			{
			case M_SPAWN:
				{
					u16 dummy16;
					P.r_begin(dummy16);
					cl_Process_Spawn(P);
				}break;
			case M_EVENT:
				{
					cl_Process_Event(dest, type, P);
				}break;
			default:
				{
					R_ASSERT(0);
				}break;
			}			
		}
	}
}

void CLevel::OnFrame	()
{
	m_feel_deny.update					();

	psDeviceFlags.set(rsDisableObjectsAsCrows,false);

	// commit events from bullet manager from prev-frame
	Device.Statistic->TEST0.Begin		();
	BulletManager().CommitEvents		();
	Device.Statistic->TEST0.End			();

	// Client receive

	Device.Statistic->netClient1.Begin();
	ClientReceive					();
	Device.Statistic->netClient1.End	();

	ProcessGameEvents	();

	if (g_mt_config.test(mtMap)) 
		Device.seqParallel.push_back	(fastdelegate::FastDelegate0<>(m_map_manager,&CMapManager::Update));
	else								
		MapManager().Update		();

	inherited::OnFrame		();

	g_pGamePersistent->Environment().SetGameTime	(GetEnvironmentGameDayTimeSec(),GetGameTimeFactor());


	CScriptProcess * levelScript = ai().script_engine().script_process(ScriptEngine::eScriptProcessorLevel);
	if (levelScript != NULL)
		levelScript->update();

	m_ph_commander->update				();
	m_ph_commander_scripts->update		();
	m_fast_updater->Update				();

	//���������� ����� ����
	Device.Statistic->TEST0.Begin		();
	BulletManager().CommitRenderSet		();
	Device.Statistic->TEST0.End			();

	// update static sounds
	if (g_mt_config.test(mtLevelSounds)) 
		Device.seqParallel.push_back	(fastdelegate::FastDelegate0<>(m_level_sound_manager,&CLevelSoundManager::Update));
	else								
		m_level_sound_manager->Update	();

	// deffer LUA-GC-STEP
	if (g_mt_config.test(mtLUA_GC))	Device.seqParallel.push_back	(fastdelegate::FastDelegate0<>(this,&CLevel::script_gc));

	else							script_gc	()	;

	//-----------------------------------------------------
	if (pStatGraphR)
	{	
		static	float fRPC_Mult = 10.0f;
		static	float fRPS_Mult = 1.0f;

		pStatGraphR->AppendItem(float(m_dwRPC)*fRPC_Mult, 0xffff0000, 1);
		pStatGraphR->AppendItem(float(m_dwRPS)*fRPS_Mult, 0xff00ff00, 0);
	};
}

void	CLevel::script_gc				()
{
	lua_gc	(ai().script_engine().lua(), LUA_GCSTEP, psLUA_GCSTEP);
}

#ifdef DEBUG
extern	Flags32	dbg_net_Draw_Flags;
#endif

void CLevel::OnRender()
{
	inherited::OnRender	();

	if (!level_game_cl_base)
		return;

	BulletManager().Render();
	HUD().RenderUI();

	draw_wnds_rects();

	#ifdef DEBUG
	ph_world->OnRender();


	if (ai().get_level_graph())
		ai().level_graph().render();

	#ifdef DEBUG_PRECISE_PATH
	test_precise_path();
	#endif

	CAI_Stalker				*stalker = smart_cast<CAI_Stalker*>(Level().CurrentEntity());
	if (stalker)
		stalker->OnRender();

	if (bDebug)	{
		for (u32 I = 0; I < Level().Objects.o_count(); I++) {
			CObject*	_O = Level().Objects.o_get_by_iterator(I);

			CPhysicObject		*physic_object = smart_cast<CPhysicObject*>(_O);
			if (physic_object)
				physic_object->OnRender();

			CSpaceRestrictor	*space_restrictor = smart_cast<CSpaceRestrictor*>	(_O);
			if (space_restrictor)
				space_restrictor->OnRender();
			CClimableObject		*climable = smart_cast<CClimableObject*>	(_O);
			if (climable)
				climable->OnRender();
			CTeamBaseZone	*team_base_zone = smart_cast<CTeamBaseZone*>(_O);
			if (team_base_zone)
				team_base_zone->OnRender();

			if (GameID() != GAME_SINGLE)
			{
				CInventoryItem* pIItem = smart_cast<CInventoryItem*>(_O);
				if (pIItem) pIItem->OnRender();
			}


			if (dbg_net_Draw_Flags.test(1 << 11)) //draw skeleton
			{
				CGameObject* pGO = smart_cast<CGameObject*>	(_O);
				if (pGO && pGO != Level().CurrentViewEntity() && !pGO->H_Parent())
				{
					if (pGO->Position().distance_to_sqr(Device.vCameraPosition) < 400.0f)
					{
						pGO->dbg_DrawSkeleton();
					}
				}
			};
		}
		//  [7/5/2005]
//		if (Server && Server->Server_game_sv_base) Server->Server_game_sv_base->OnRender();
		//  [7/5/2005]
		ObjectSpace.dbgRender();

		//---------------------------------------------------------------------
		UI().Font().pFontStat->OutSet(170, 630);
		UI().Font().pFontStat->SetHeight(16.0f);
		UI().Font().pFontStat->SetColor(0xffff0000);

		if (Server)UI().Font().pFontStat->OutNext("Client Objects:      [%d]", Server->GetEntitiesNum());
		UI().Font().pFontStat->OutNext("Server Objects:      [%d]", Objects.o_count());
		UI().Font().pFontStat->SetHeight(8.0f);
		//---------------------------------------------------------------------
	}

	if (bDebug) {
		DBG().draw_object_info();
		DBG().draw_text();
		DBG().draw_level_info();
	}
	#endif

	#ifdef DRENDER
	debug_renderer().render();
	#endif

	#ifdef LOG_PLANNER
	if (psAI_Flags.is(aiVision)) {
		for (u32 I = 0; I < Level().Objects.o_count(); I++) {
			CObject						*object = Objects.o_get_by_iterator(I);
			CAI_Stalker					*stalker = smart_cast<CAI_Stalker*>(object);
			if (!stalker)
				continue;
			stalker->dbg_draw_vision();
		}
	}


	if (psAI_Flags.test(aiDrawVisibilityRays)) {
		for (u32 I = 0; I < Level().Objects.o_count(); I++) {
			CObject						*object = Objects.o_get_by_iterator(I);
			CAI_Stalker					*stalker = smart_cast<CAI_Stalker*>(object);
			if (!stalker)
				continue;

			stalker->dbg_draw_visibility_rays();
		}
	}
	#endif
}

void CLevel::OnEvent(EVENT E, u64 P1, u64 /**P2/**/)
{
	if (E == eEntitySpawn)	{
		char	Name[128];	Name[0] = 0;
		sscanf(LPCSTR(P1), "%s", Name);
		Level().g_cl_Spawn(Name, 0xff, M_SPAWN_OBJECT_LOCAL, Fvector().set(0, 0, 0));
	}
	else return;
}

void	CLevel::net_Save(LPCSTR name)		// Game Save
{
	// 1. Create stream
	CMemoryWriter			fs;

	// 2. Description
	fs.open_chunk(fsSLS_Description);
	fs.w_stringZ(net_SessionName());
	fs.close_chunk();

	// 3. Server state
	fs.open_chunk(fsSLS_ServerState);
	Server->SLS_Save(fs);
	fs.close_chunk();

	// Save it to file
	fs.save_to(name);
}


ALife::_TIME_ID CLevel::GetGameTime()
{
	return (level_game_cl_base->GetGameTime());
}

ALife::_TIME_ID CLevel::GetEnvironmentGameTime()
{
	return (level_game_cl_base->GetEnvironmentGameTime());
}

#include "game_sv_base.h"
#include "game_cl_base.h"

void CLevel::SetGameTime(u32 new_hours, u32 new_mins)// gr1ph
{
	float time_factor = Level().GetGameTimeFactor();
	u32 year = 1, month = 0, day = 0, hours = 0, mins = 0, secs = 0, milisecs = 0;
	split_time(Level().GetGameTime(), year, month, day, hours, mins, secs, milisecs);
	u64 new_time = generate_time(year, month, day, new_hours, new_mins, secs, milisecs);
	if (Level().level_game_cl_base){ Msg("client_game"); }
	if (Level().Server->Server_game_sv_base){ Msg("server_game"); }
	Level().Server->Server_game_sv_base->SetGameTimeFactor(new_time, time_factor);
	Level().Server->Server_game_sv_base->SetEnvironmentGameTimeFactor(new_time, time_factor);
	Level().level_game_cl_base->SetEnvironmentGameTimeFactor(new_time, time_factor);
	Level().level_game_cl_base->SetGameTimeFactor(new_time, time_factor);
}

u8 CLevel::GetDayTime() 
{ 
	u32 dummy32;
	u32 hours;
	GetGameDateTime(dummy32, dummy32, dummy32, hours, dummy32, dummy32, dummy32);
	R_ASSERT(hours<256);
	return	u8(hours); 
}

float CLevel::GetGameDayTimeSec()
{
	return	(float(s64(GetGameTime() % (24*60*60*1000)))/1000.f);
}

u32 CLevel::GetGameDayTimeMS()
{
	return	(u32(s64(GetGameTime() % (24*60*60*1000))));
}

float CLevel::GetEnvironmentGameDayTimeSec()
{
	return	(float(s64(GetEnvironmentGameTime() % (24*60*60*1000)))/1000.f);
}

void CLevel::GetGameDateTime	(u32& year, u32& month, u32& day, u32& hours, u32& mins, u32& secs, u32& milisecs)
{
	split_time(GetGameTime(), year, month, day, hours, mins, secs, milisecs);
}

void CLevel::GetGameTimeHour(u32& hours)
{
	u32 dummy1;
	split_time(GetGameTime(), dummy1, dummy1, dummy1, hours, dummy1, dummy1, dummy1);
}

void CLevel::GetGameTimeMinute(u32& minute)
{
	u32 dummy1;
	split_time(GetGameTime(), dummy1, dummy1, dummy1, dummy1, minute, dummy1, dummy1);
}

float CLevel::GetGameTimeFactor()
{
	return (level_game_cl_base->GetGameTimeFactor());
}

void CLevel::SetGameTimeFactor(const float fTimeFactor)
{
	level_game_cl_base->SetGameTimeFactor(fTimeFactor);
}

void CLevel::SetGameTimeFactor(ALife::_TIME_ID GameTime, const float fTimeFactor)
{
	level_game_cl_base->SetGameTimeFactor(GameTime, fTimeFactor);
}
void CLevel::SetEnvironmentGameTimeFactor(u64 const& GameTime, float const& fTimeFactor)
{
	if (!level_game_cl_base)
		return;
	level_game_cl_base->SetEnvironmentGameTimeFactor(GameTime, fTimeFactor);
}

#include "../IGame_Persistent.h"

GlobalFeelTouch::GlobalFeelTouch()
{
}

GlobalFeelTouch::~GlobalFeelTouch()
{
}

struct delete_predicate_by_time : public std::binary_function<Feel::Touch::DenyTouch, DWORD, bool>
{
	bool operator () (Feel::Touch::DenyTouch const & left, DWORD const expire_time) const
	{
		if (left.Expire <= expire_time)
			return true;
		return false;
	};
};

struct objects_ptrs_equal : public std::binary_function<Feel::Touch::DenyTouch, CObject const *, bool>
{
	bool operator() (Feel::Touch::DenyTouch const & left, CObject const * const right) const
	{
		if (left.O == right)
			return true;
		return false;
	}
};

void GlobalFeelTouch::update()
{
	//we ignore P and R arguments, we need just delete evaled denied objects...
	xr_vector<Feel::Touch::DenyTouch>::iterator new_end = 
		std::remove_if(feel_touch_disable.begin(), feel_touch_disable.end(), 
			std::bind2nd(delete_predicate_by_time(), Device.dwTimeGlobal));
	feel_touch_disable.erase(new_end, feel_touch_disable.end());
}

bool GlobalFeelTouch::is_object_denied(CObject const * O)
{
	/*Fvector temp_vector;
	feel_touch_update(temp_vector, 0.f);*/
	if (std::find_if(feel_touch_disable.begin(), feel_touch_disable.end(),
		std::bind2nd(objects_ptrs_equal(), O)) == feel_touch_disable.end())
	{
		return false;
	}
	return true;
}

#include "../../xrNetServer/NET_AuthCheck.h"
struct path_excluder_predicate
{
	explicit path_excluder_predicate(xr_auth_strings_t const * ignore) :
		m_ignore(ignore)
	{
	}
	bool xr_stdcall is_allow_include(LPCSTR path)
	{
		if (!m_ignore)
			return true;

		return allow_to_include_path(*m_ignore, path);
	}
	xr_auth_strings_t const *	m_ignore;
};

void CLevel::ReloadEnvironment()
{
	g_pGamePersistent->DestroyEnvironment();
	Msg("---Environment destroyed");
	Msg("---Start to destroy configs");
	CInifile** s = (CInifile**)(&pSettings);
	xr_delete(*s);
	xr_delete(pGameIni);
	Msg("---Start to rescan configs");
	FS.get_path("$game_config$")->m_Flags.set(FS_Path::flNeedRescan, TRUE);
	FS.get_path("$game_scripts$")->m_Flags.set(FS_Path::flNeedRescan, TRUE);
	FS.rescan_pathes();

	Msg("---Start to create configs");
	string_path					fname;
	FS.update_path(fname, "$game_config$", "system.ltx");
	Msg("---Updated path to system.ltx is %s", fname);

	pSettings = xr_new <CInifile>(fname, TRUE);
	CHECK_OR_EXIT(0 != pSettings->section_count(), make_string("Cannot find file %s.\nReinstalling application may fix this problem.", fname));

	xr_auth_strings_t			tmp_ignore_pathes;
	xr_auth_strings_t			tmp_check_pathes;
	fill_auth_check_params(tmp_ignore_pathes, tmp_check_pathes);

	path_excluder_predicate			tmp_excluder(&tmp_ignore_pathes);
	CInifile::allow_include_func_t	tmp_functor;
	tmp_functor.bind(&tmp_excluder, &path_excluder_predicate::is_allow_include);
	pSettingsAuth = xr_new <CInifile>(
		fname,
		TRUE,
		TRUE,
		FALSE,
		0,
		tmp_functor
		);

	FS.update_path(fname, "$game_config$", "game.ltx");
	pGameIni = xr_new <CInifile>(fname, TRUE);
	CHECK_OR_EXIT(0 != pGameIni->section_count(), make_string("Cannot find file %s.\nReinstalling application may fix this problem.", fname));

	Msg("---Create environment");
	g_pGamePersistent->CreateEnvironment();

	Msg("---Call level_weathers.restart_weather_manager");
	luabind::functor<void>	lua_function;
	string256		fn;
	xr_strcpy(fn, "level_weathers.restart_weather_manager");
	R_ASSERT2(ai().script_engine().functor<void>(fn, lua_function), make_string("Can't find function %s", fn));
	lua_function();
	Msg("---Done");
}

