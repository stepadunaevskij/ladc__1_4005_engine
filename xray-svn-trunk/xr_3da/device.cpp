#include "stdafx.h"
#include "../xrCDB/frustum.h"

#pragma warning(disable:4995)

#define MMNOSOUND
#define MMNOMIDI
#define MMNOAUX
#define MMNOMIXER
#define MMNOJOY
#include <mmsystem.h>
#include <d3dx9.h>

#pragma warning(default:4995)


#include "x_ray.h"
#include "render.h"

// must be defined before include of FS_impl.h
#define INCLUDE_FROM_ENGINE
#include "../xrCore/FS_impl.h"
 
#include "igame_persistent.h"

#pragma comment( lib, "d3dx9.lib"		)

ENGINE_API CRenderDevice Device;
ENGINE_API CLoadScreenRenderer load_screen_renderer;
ENGINE_API CTimer loading_save_timer;

ENGINE_API bool loading_save_timer_started				= false;
ENGINE_API bool prefetching_in_progress					= false;
ENGINE_API BOOL g_bRendering							= FALSE;
ENGINE_API bool CheckEngineFfreezing					= true;

ENGINE_API float VIEWPORT_NEAR							= 0.2f;

ENGINE_API Mutex GameSaveMutex;

BOOL		g_bLoaded = FALSE;
ref_light	precache_light = 0;

CTimer MainThreadTimer, Thread1Timer;

extern void CheckPrivilegySlowdown();


bool CRenderDevice::IsR2Active()
{
	return (psDeviceFlags.test(rsR2)) ? true : false;
}

bool CRenderDevice::IsR3Active()
{
	return (psDeviceFlags.test(rsR3)) ? true : false;
}

bool CRenderDevice::IsR4Active()
{
	return (psDeviceFlags.test(rsR4)) ? true : false;
}

void CRenderDevice::Clear	()
{
	m_pRender->Clear();
}

BOOL CRenderDevice::Begin()
{
	switch (m_pRender->GetDeviceState())
	{
	case IRenderDeviceRender::dsOK:
		break;

	case IRenderDeviceRender::dsLost:
		// If the device was lost, do not render until we get it back
		Sleep(100);
		return FALSE;
		break;

	case IRenderDeviceRender::dsNeedReset:
		// Check if the device is ready to be reset
		Reset();
		break;

	default:
		R_ASSERT(0);
	}

	m_pRender->Begin();

	FPU::m24r();
	g_bRendering = TRUE;
	return		TRUE;
}


void CRenderDevice::End		(void)
{
	if (dwPrecacheFrame)
	{
		::Sound->set_master_volume	(0.f);
		dwPrecacheFrame	--;
		if (0==dwPrecacheFrame)
		{
			//Gamma.Update		();
			//m_pRender->updateGamma();

			if(precache_light) precache_light->set_active	(false);
			if(precache_light) precache_light.destroy		();
			::Sound->set_master_volume						(1.f);

			m_pRender->ResourcesDestroyNecessaryTextures	();
			Memory.mem_compact								();
			Msg												("* MEMORY USAGE: %d K",Statistic->GetTotalRAMConsumption()/1024);
			Msg												("* End of synchronization A[%d] R[%d]",b_is_Active, b_is_Ready);
			if (loading_save_timer_started)
			{ 
				Msg												("* Game Loading Time: %d ms", loading_save_timer.GetElapsed_ms());
				loading_save_timer_started = false;
			}

#ifdef FIND_CHUNK_BENCHMARK_ENABLE
			g_find_chunk_counter.flush();
#endif

			CheckPrivilegySlowdown							();
			
			if(g_pGamePersistent->GameType()==1) // hack
			{
				WINDOWINFO	wi;
				GetWindowInfo(m_hWnd,&wi);
				if(wi.dwWindowStatus!=WS_ACTIVECAPTION)
					Pause(TRUE,TRUE,TRUE,"application start");
			}
		}
	}

	g_bRendering		= FALSE;

	m_pRender->End();
}


//Auxilary thread that executes delayed stuff while Main thread is buisy with Render
bool thread1ready;

void CRenderDevice::AuxThread_1(void *context)
{
	CRenderDevice& device = *static_cast<CRenderDevice*>(context);
	while (true) 
	{
		device.syncProcessFrame.Wait(); // Wait until main thread allows aux thread 1 to execute

		if (device.mt_bMustExit) 
		{
			//device.mt_bMustExit = FALSE;
			device.Aux1Exit_Sync.Set();
			return;
		}
		// we are ok to execute
		Thread1Timer.Start();
		thread1ready = false;
		device.Statistic->AuxThreadObjects = device.seqParallel.size();

		GameSaveMutex.lock();
		for (u32 pit = 0; pit < device.seqParallel.size(); pit++){
			device.seqParallel[pit]();
		}
		device.seqParallel.clear_not_free();
		device.seqFrameMT.Process(rp_Frame);
		GameSaveMutex.unlock();

		device.Statistic->Thread1Time = Thread1Timer.GetElapsed_sec()*1000.f;
		thread1ready = true;

		device.syncFrameDone.Set(); // tell main thread that aux thread 1 has finished its job
	}
}


//Auxilary thread that executes delayed stuff not depending on Main thread (Just a template. Not used yet)
void CRenderDevice::AuxThread_2(void *context)
{
	CRenderDevice& device = *static_cast<CRenderDevice*>(context);
	while (true)
	{
		Sleep(1);
		if (device.mt_bMustExit) {
			device.Aux2Exit_Sync.Set();
			return;
		}

		//Msg("AuxThread_2() size %u", device.seqAuxThread2.size());
		if (device.seqAuxThread2.size() > 0)
		{

			device.Aux2PoolProtection.lock(); //Protect pool, while copying it to temporary one

			xr_vector<fastdelegate::FastDelegate0<>>temp_copy; // make a copy of corrent pool, so that the access to pool is not raced by threads
			temp_copy = device.seqAuxThread2;

			device.Aux2PoolProtection.unlock();


			for (u32 i = 0; i < temp_copy.size(); ++i)
			{
				CTimer time; time.Start();
				Msg("Processeing %u", i);

				temp_copy[i]();

				Msg("Processed %u, Saved processing time = %f, frame finnished = %u", i, time.GetElapsed_sec()*1000.f, device.dwFrame);
			}


			device.Aux2PoolProtection.lock(); //Protect pool, while thread cleans executed voids

			for (auto it = temp_copy.begin(); it != temp_copy.end(); ++it)
			{
				const fastdelegate::FastDelegate0<> &delegate = *it;
				xr_vector<fastdelegate::FastDelegate0<> >::iterator I = std::find(
					device.seqAuxThread2.begin(),
					device.seqAuxThread2.end(),
					delegate);

				if (I != device.seqAuxThread2.end())
				{
					Msg("Erasing");
					device.seqAuxThread2.erase(I);
				}
			}

			device.Aux2PoolProtection.unlock();


		}
	}
}


Mutex SyncThreads_Freeze_and_Main;
CTimer FreezeTimer;
void mt_FreezeThread(void *ptr)
{
	float freezetime = 0.f;
	u16 repeatcheck = 500;
	FreezeTimer.Start(); // initialization

	while (true)
	{
		if (CheckEngineFfreezing)
		{
			if (g_loading_events.size())freezetime = 15000.0f;
			else freezetime = 5000.0f;

			repeatcheck = 500;
			SyncThreads_Freeze_and_Main.lock();
			float elapsedtime = FreezeTimer.GetElapsed_sec()*1000.f;
			SyncThreads_Freeze_and_Main.unlock();
			if (elapsedtime > freezetime)
			{
				Msg("---Engine is freezed, saving log [%f]", elapsedtime);
				FlushLog();
				repeatcheck = 5000;
			}
		}
		Sleep(repeatcheck);
	}
}

#include "igame_level.h"
void CRenderDevice::PreCache	(u32 amount, bool b_draw_loadscreen, bool b_wait_user_input)
{
	if (m_pRender->GetForceGPU_REF()) amount=0;

	// Msg			("* PCACHE: start for %d...",amount);
	dwPrecacheFrame	= dwPrecacheTotal = amount;
	if (amount && !precache_light && g_pGameLevel && g_loading_events.empty()) {
		precache_light					= ::Render->light_create();
		precache_light->set_shadow		(false);
		precache_light->set_position	(vCameraPosition);
		precache_light->set_color		(255,255,255);
		precache_light->set_range		(5.0f);
		precache_light->set_active		(true);
	}

	if(amount && b_draw_loadscreen && load_screen_renderer.b_registered==false)
	{
		load_screen_renderer.start	(b_wait_user_input);
	}
}

extern BOOL show_FPS_only;
extern BOOL show_engine_timers;
extern BOOL show_render_times;
float ps_frame_rate_ms = 0.f;

ENGINE_API xr_list<LOADING_EVENT>			g_loading_events;

// Basicly, this is a main thread of our app
void CRenderDevice::on_idle()
{
	MainThreadTimer.Start();
	SyncThreads_Freeze_and_Main.lock();
	FreezeTimer.Start();
	SyncThreads_Freeze_and_Main.unlock();

	if (!b_is_Ready) 
	{
		Sleep(100);
		return;
	}

	if (psDeviceFlags.test(rsStatistic) || show_FPS_only == TRUE || show_engine_timers == TRUE)
		g_bEnableStatGather = TRUE;
	else
		g_bEnableStatGather	= FALSE;

	// Process next Loading Event and return
	if (g_loading_events.size())
	{
		if( g_loading_events.front()() )
			g_loading_events.pop_front();

		pApp->LoadDraw();
		return;
	}

	// Cap frame rate to user specified value or if scene rendering is skipped
	bool b_not_rendering_scene = (g_pGamePersistent && g_pGamePersistent->SceneRenderingBlocked());
	frameTimeDelta_ = rateControlingTimer_.GetElapsed_sec() - previousFrameTime_;

	if (b_not_rendering_scene || ps_frame_rate_ms > 0.f){
		float device_rate_ms = b_not_rendering_scene ? 6.94f : ps_frame_rate_ms;
		if (device_rate_ms >= frameTimeDelta_ * 1000.f)
		{
			return;
		}
	}

	// Measure FPS
	Statistic->fDeviceMeasuredFPS = 1.f / frameTimeDelta_;
	previousFrameTime_ = rateControlingTimer_.GetElapsed_sec();

	// Do!
	FrameMove();

	// Precache
	if (dwPrecacheFrame)
	{
		float factor = float(dwPrecacheFrame)/float(dwPrecacheTotal);
		float angle = PI_MUL_2 * factor;
		vCameraDirection.set(_sin(angle), 0, _cos(angle));
		vCameraDirection.normalize();
		vCameraTop.set(0, 1, 0);
		vCameraRight.crossproduct(vCameraTop,vCameraDirection);
		mView.build_camera_dir(vCameraPosition, vCameraDirection, vCameraTop);
	}

	// Matrices
	mFullTransform.mul(mProject, mView);
	m_pRender->SetCacheXform(mView, mProject);
	D3DXMatrixInverse((D3DXMATRIX*)&mInvFullTransform, 0, (D3DXMATRIX*)&mFullTransform);

	vCameraPosition_saved = vCameraPosition;
	mFullTransform_saved = mFullTransform;
	mView_saved = mView;
	mProject_saved = mProject;

	syncProcessFrame.Set(); // allow aux thread 1 to do its job

	Statistic->RenderTOTAL_Real.FrameStart	();
	Statistic->RenderTOTAL_Real.Begin		();
	if (b_is_Active && Begin())
	{
		seqRender.Process(rp_Render);
		if (psDeviceFlags.test(rsCameraPos) || psDeviceFlags.test(rsStatistic) || show_FPS_only == TRUE || show_engine_timers == TRUE || show_render_times == TRUE || Statistic->errors.size())
			Statistic->Show();
		End();
	}
	Statistic->RenderTOTAL_Real.End();
	Statistic->RenderTOTAL_Real.FrameEnd();
	Statistic->RenderTOTAL.accum = Statistic->RenderTOTAL_Real.accum;

	if (!thread1ready) {
		Statistic->MainThreadSuspendCounter += 1;
	}

	syncFrameDone.Wait(); // wait until aux thread 1 finish its job

	Statistic->MainThreadTime = MainThreadTimer.GetElapsed_sec()*1000.f;
}


void CRenderDevice::message_loop()
{
	MSG msg;
    PeekMessage(&msg, NULL, 0U, 0U, PM_NOREMOVE );
	while (msg.message != WM_QUIT)
	{
		if (PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage	(&msg);
			continue;
		}
		on_idle();
    }
}


void CRenderDevice::Run			()
{
	g_bLoaded		= FALSE;
	Log				("Starting engine...");
	thread_name		("X-RAY Primary thread");

	// Startup timers and calculate timer delta
	dwTimeGlobal				= 0;
	Timer_MM_Delta				= 0;

	previousFrameTime_			= 0.f;
	frameTimeDelta_				= 0.f;
	rateControlingTimer_.Start();

	{
		u32 time_mm			= timeGetTime	();
		while (timeGetTime()==time_mm);			// wait for next tick
		u32 time_system		= timeGetTime	();
		u32 time_local		= TimerAsync	();
		Timer_MM_Delta		= time_system-time_local;
	}

	// Start all threads
	mt_bMustExit				= FALSE;
	thread_spawn				(AuxThread_1,"X-RAY Secondary thread",0,this);
//	thread_spawn				(AuxThread_2,"X-RAY Secondary thread 2",0,this);
	thread_spawn				(mt_FreezeThread,"Freeze detecting thread",0,0);

	// Message cycle
	seqAppStart.Process			(rp_AppStart);

	m_pRender->ClearTarget		();

	message_loop				();

	seqAppEnd.Process		(rp_AppEnd);

	// Stop Aux-Thread
	mt_bMustExit = TRUE;
	syncProcessFrame.Set();
	Aux1Exit_Sync.Wait();
//	Aux2Exit_Sync.Wait();
	SoundsThreadExit_Sync.Wait();
	//while (mt_bMustExit) Sleep(0);

}
u32 app_inactive_time		= 0;
u32 app_inactive_time_start = 0;

void ProcessLoading(RP_FUNC *f);
void CRenderDevice::FrameMove()
{
	dwFrame			++;

	dwTimeContinual	= TimerMM.GetElapsed_ms() - app_inactive_time;

	if (psDeviceFlags.test(rsConstantFPS))	{
		fTimeDelta		=	0.033f;			
		fTimeGlobal		+=	0.033f;
		dwTimeDelta		=	33;
		dwTimeGlobal	+=	33;
	} else {
		// Timer
		float fPreviousFrameTime = Timer.GetElapsed_sec(); Timer.Start();	// previous frame
		fTimeDelta = 0.1f * fTimeDelta + 0.9f*fPreviousFrameTime;			// smooth random system activity - worst case ~7% error
		if (fTimeDelta>.1f)    
			fTimeDelta = .1f;							// limit to 15fps minimum

		if (fTimeDelta <= 0.f) 
			fTimeDelta = EPS_S + EPS_S;					// limit to 15fps minimum

		if(Paused())	
			fTimeDelta = 0.0f;

//		u64	qTime		= TimerGlobal.GetElapsed_clk();
		fTimeGlobal		= TimerGlobal.GetElapsed_sec(); //float(qTime)*CPU::cycles2seconds;
		u32	_old_global	= dwTimeGlobal;
		dwTimeGlobal	= TimerGlobal.GetElapsed_ms	();	//u32((qTime*u64(1000))/CPU::cycles_per_second);
		dwTimeDelta		= dwTimeGlobal-_old_global;
	}

	// Frame move
	Statistic->EngineTOTAL.Begin();

	ProcessLoading				(rp_Frame);

	Statistic->EngineTOTAL.End();
}

void ProcessLoading				(RP_FUNC *f)
{
	Device.seqFrame.Process				(rp_Frame);
	g_bLoaded							= TRUE;
}

ENGINE_API BOOL bShowPauseString = TRUE;
#include "IGame_Persistent.h"

void CRenderDevice::Pause(BOOL bOn, BOOL bTimer, BOOL bSound, LPCSTR reason)
{
	static int snd_emitters_ = -1;

	if (g_bBenchmark)	return;


#ifdef DEBUG
//if (psDeviceFlags.test(rsDebugMsg)) 
	//Msg("pause [%s] timer=[%s] sound=[%s] reason=%s",bOn?"ON":"OFF", bTimer?"ON":"OFF", bSound?"ON":"OFF", reason);
#endif // DEBUG


	if(bOn)
	{
		if(!Paused())						
			bShowPauseString				= 
#ifdef DEBUG
				!xr_strcmp(reason, "li_pause_key_no_clip")?	FALSE:
#endif // DEBUG
				TRUE;

		if( bTimer && (!g_pGamePersistent || g_pGamePersistent->CanBePaused()) )
		{
			g_pauseMngr.Pause				(TRUE);
#ifdef DEBUG
			if(!xr_strcmp(reason, "li_pause_key_no_clip"))
				TimerGlobal.Pause				(FALSE);
#endif // DEBUG
		}
	
		if (bSound && ::Sound) {
			snd_emitters_ =					::Sound->pause_emitters(true);
#ifdef DEBUG
//			Log("snd_emitters_[true]",snd_emitters_);
#endif // DEBUG
		}
	}else
	{
		if( bTimer && g_pauseMngr.Paused() )
		{
			fTimeDelta						= EPS_S + EPS_S;
			g_pauseMngr.Pause				(FALSE);
		}
		
		if(bSound)
		{
			if(snd_emitters_>0) //avoid crash
			{
				snd_emitters_ =				::Sound->pause_emitters(false);
#ifdef DEBUG
//				Log("snd_emitters_[false]",snd_emitters_);
#endif // DEBUG
			}else {
#ifdef DEBUG
				Log("Sound->pause_emitters underflow");
#endif // DEBUG
			}
		}
	}


}


BOOL CRenderDevice::Paused()
{
	return g_pauseMngr.Paused();
};


void CRenderDevice::OnWM_Activate(WPARAM wParam, LPARAM lParam)
{
	u16 fActive						= LOWORD(wParam);
	BOOL fMinimized					= (BOOL) HIWORD(wParam);
	BOOL bActive					= ((fActive!=WA_INACTIVE) && (!fMinimized))?TRUE:FALSE;
	
	if (bActive!=Device.b_is_Active)
	{
		Device.b_is_Active			= bActive;

		if (Device.b_is_Active)	
		{
			Device.seqAppActivate.Process(rp_AppActivate);
			app_inactive_time		+= TimerMM.GetElapsed_ms() - app_inactive_time_start;

			ShowCursor			(FALSE);
		}else	
		{
			app_inactive_time_start	= TimerMM.GetElapsed_ms();
			Device.seqAppDeactivate.Process(rp_AppDeactivate);
			ShowCursor				(TRUE);
		}
	}
}


void	CRenderDevice::AddSeqFrame			( pureFrame* f, bool mt )
{
	if ( mt )	
		seqFrameMT.Add		(f,REG_PRIORITY_HIGH);
	else								
		seqFrame.Add		(f,REG_PRIORITY_LOW);

}


void	CRenderDevice::RemoveSeqFrame	( pureFrame* f )
{
	seqFrameMT.Remove	( f );
	seqFrame.Remove		( f );
}


CLoadScreenRenderer::CLoadScreenRenderer()
:b_registered(false)
{}


void CLoadScreenRenderer::start(bool b_user_input) 
{
	Device.seqRender.Add			(this, 0);
	b_registered					= true;
	b_need_user_input				= b_user_input;
}


void CLoadScreenRenderer::stop()
{
	if(!b_registered)				return;
	Device.seqRender.Remove			(this);
	pApp->destroy_loading_shaders	();
	b_registered					= false;
	b_need_user_input				= false;
}


void CLoadScreenRenderer::OnRender() 
{
	pApp->load_draw_internal();
}